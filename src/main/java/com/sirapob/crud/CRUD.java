/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.sirapob.crud;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class CRUD {

    public static void main(String[] args) {
        User admin = new User("admin","Administrator","pass@1234",'M','A');
        User user1 = new User("user1","User 1","pass@1234",'M','U');
        User user2 = new User("user2","User 2","pass@1234",'F','A');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("userArr");
        for(int i=0;i<userArr.length;i++){
            System.out.println(userArr[i]);
        }
        System.out.println("ArrayList");
        ArrayList<User> userList = new ArrayList<User>();
        userList.add(admin);
        System.out.println(userList.get(userList.size()-1)+ "list size =" + userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size()-1)+ "list size =" + userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size()-1)+ "list size =" + userList.size());
        for(int i=0;i<userList.size();i++){
            System.out.println(userList.get(i));
        }
    }
    
}
